package org.elstest;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication   
@ComponentScan({"org.elstest"})
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class ElstestGraphApplication {

  public static void main(String[] args) {
    new SpringApplicationBuilder(ElstestGraphApplication.class).properties("spring.config.name:elstest").build().run(ElstestGraphApplication.class, args);
  }

}
