package org.elstest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elstest.data.Product;
import org.elstest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/elstest")
@SessionAttributes("login")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(path = "/search")
    public  @ResponseBody
    String searchProduct (@RequestParam("productname") String productName) throws IOException {

        // build response
        Product product = productService.findProductByTitle(productName);

        // response data
        return new ObjectMapper().writeValueAsString(product);

    }

    @GetMapping(path = "/listproducts")
    public  @ResponseBody
    String listProducts () throws IOException {

        // build response
        List<String> listProducts = productService.getProductTitles();

        // response data
        return new ObjectMapper().writeValueAsString(listProducts);

    }


}
