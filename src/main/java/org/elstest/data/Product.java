package org.elstest.data;

import lombok.Data;
/**
 * Simplified flattened version of product data for demo purposes
 *
 */
@Data
public class Product {

    private final String notificationType;
    private final String productIdentifier;
    private final String productForm;
    private final String title;
    private final String contributor;
    private final String languageCode;
    private final String imprintName;
    private final String publicationDate;

    private Product(ProductBuilder builder){
        this.notificationType = builder.notificationType;
        this.productIdentifier = builder.productIdentifier;
        this.productForm = builder.productForm;
        this.title = builder.title;
        this.contributor = builder.contributor;
        this.languageCode = builder.languageCode;
        this.imprintName = builder.imprintName;
        this.publicationDate = builder.publicationDate;
    }

    public static class ProductBuilder {

        private final String productForm;
        private final String title;

        private String notificationType;
        private String productIdentifier;
        private String contributor;
        private String languageCode;
        private String imprintName;
        private String publicationDate;

        public ProductBuilder(String title, String productForm){
            // Concatenate 2 params to reduce dupes
            StringBuilder sbTitle = new StringBuilder(title);
            sbTitle.append(" - ");
            sbTitle.append(productForm);
            this.title = sbTitle.toString();
            this.productForm = productForm;
        }

        public ProductBuilder addNotificationType(String notificationType){
            this.notificationType = notificationType;
            return this;
        }
        public ProductBuilder addProductIdentifier(String productIdentifier){
            this.productIdentifier = productIdentifier;
            return this;
        }
        public ProductBuilder addContributor(String contributor){
            this.contributor = contributor;
            return this;
        }
        public ProductBuilder addLanguageCode(String languageCode){
            this.languageCode = languageCode;
            return this;
        }
        public ProductBuilder addImprintName(String imprintName){
            this.imprintName = imprintName;
            return this;
        }

        public ProductBuilder addPublicationDate(String publicationDate){
            this.publicationDate = publicationDate;
            return this;
        }

        public Product build(){
            return new Product(this);
        }

    }
}
