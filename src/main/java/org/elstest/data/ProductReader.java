package org.elstest.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
* Read product data from json file specified in properties file
*/
@Component
public class ProductReader {

    private static final Logger LOG = LoggerFactory.getLogger(ProductReader.class);

    private final Map<String, Product> mapProducts = new HashMap<>();

    @Autowired
    public ProductReader(@Value("${json.datafile.products}") String jsonFileName) {

        //check for file existence
        if (!Files.exists(Paths.get(jsonFileName))) {
            LOG.error("Invalid JSON file name: " + jsonFileName);
            throw new IllegalArgumentException("Invalid JSON file name: " + jsonFileName);
        }

       try {

           final JsonNode node = new ObjectMapper().readTree(new File(jsonFileName));
           final JsonNode worksNode = node.get("worksById");
           final List<Product> allProducts = new ArrayList<>();

           // pick out a smaple of the product fields, load into a Product object then add to List of products
           Iterator<JsonNode> elements = worksNode.iterator();
           while (elements.hasNext()){
               JsonNode element = elements.next();
               Product.ProductBuilder builder = new Product.ProductBuilder(checkAndClean(element.path("Title").get("TitleText")),
                       checkAndClean(element.get("ProductForm")));

               JsonNode contribNode = element.path("Contributor").get("KeyNames");
               // if not found then there will be multiple
               String contributor;
               if (contribNode == null) {
                   int num = element.path("Contributor").size();
                   StringBuilder contribs = new StringBuilder();
                   for (int i = 0; i < num; i++){
                       contribs.append(checkAndClean(element.path("Contributor").get(i).get("KeyNames")));
                       contribs.append(i < num-1 ? "/" : "");
                   }
                   contributor = contribs.toString();
               } else
                   contributor = checkAndClean(contribNode);

               builder.addNotificationType(checkAndClean(element.get("NotificationType")))
                       .addProductIdentifier(checkAndClean(element.path("ProductIdentifier").get(0).get("IDValue")))
                       .addContributor(contributor)
                       .addLanguageCode(checkAndClean(element.path("Language").get("LanguageCode")))
                       .addImprintName(checkAndClean(element.path("Imprint").get("ImprintName")))
                       .addPublicationDate(checkAndClean(element.get("PublicationDate")));

               allProducts.add(builder.build());

           }

           // use lower case version of title for keys to enable case-insensitive searches
           allProducts.forEach(p-> mapProducts.put(p.getTitle().toLowerCase(), p));

           LOG.info("[" + allProducts.size() + "] products successfully loaded");

        } catch (IOException e){
           LOG.error("Error loading JSON file " + jsonFileName, e);
        }
    }
    public Product getProductByTitle(String title) {

        Product selectedProduct = mapProducts.get(title.toLowerCase());

        // if not found try partial match
        if (selectedProduct == null) {
            Optional<Product> result = mapProducts.values().stream().filter(p -> p.getTitle().toLowerCase().startsWith(title.toLowerCase())).findFirst();
            if (result.isPresent()) selectedProduct = result.get();
        }

        LOG.info("Searched for [{}] - found [{}]", title, selectedProduct==null ? "not found" : selectedProduct.getTitle());

        return selectedProduct;

    }

    public List<String> getProductTitles(){
        LOG.info("Returning list of {} product titles", mapProducts.size());
        // present original case titles on screen
        return new ArrayList<>(mapProducts.values().stream().map(Product::getTitle).collect(Collectors.toList()));
    }

    /**
     * Utility method to check for nulls and strip out extraneous quotes
     */
    private String checkAndClean(JsonNode node){
        return node == null ? "" : removeQuotes(node.toString());
    }

    private String removeQuotes(String str) {
        if (str.length() == 0) return str;

        int len = str.length();
        if (str.startsWith("\"") && str.endsWith("\""))
            return str.substring(1, len-1);
        else
            return str;
    }
}
