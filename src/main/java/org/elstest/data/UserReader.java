package org.elstest.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * <code>StorageReader</code> is a immutable utility class used for handling data storage of users in json
 * One connection to db is used by all users
 * @author Ian Gatenby
 * @version 1.0
 */
@Component
public class UserReader {

    private static final Logger LOG = LoggerFactory.getLogger(UserReader.class);

    private final Map<String, User> mapUsers = new HashMap<>();

    @Autowired
    public UserReader(@Value("${json.datafile.users}") String jsonFileName) {

        //check for file existence
        if (!Files.exists(Paths.get(jsonFileName))) throw new IllegalArgumentException("Invalid JSON file name: " + jsonFileName);

        try {
            UserList userList = new ObjectMapper().readValue(new File(jsonFileName), UserList.class);
            userList.getUserList().forEach(s -> mapUsers.put(s.getUsername(), s));
            LOG.info("[" + mapUsers.size() + "] users successfully loaded");

        } catch (IOException e){
            LOG.error("Error loading JSON file " + jsonFileName, e);
        }
    }

    public User findUserByName(String userName) {

        User selectedUser = mapUsers.get(userName);

        if (selectedUser != null)
            LOG.info("Successful login - username {}", userName);
        else
            LOG.info("Attempted login - username {} not found", userName);

        return selectedUser;

    }

    public List<User> getAllUsers() {

        return new ArrayList(mapUsers.values());
    }

    static class UserList {

        @JsonProperty
        private List<User> dat;

        public List<User> getUserList() {
            return new ArrayList<>(dat);
        }

    }


}
