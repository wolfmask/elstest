package org.elstest.service;

import org.elstest.data.Product;
import org.elstest.data.ProductReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private ProductReader productReader;

    @Autowired
    public ProductService(ProductReader productReader){
        this.productReader = productReader;
    }

    public Product findProductByTitle(String title){

        return productReader.getProductByTitle(title);
    }

    public List<String> getProductTitles(){
        return productReader.getProductTitles();
    }
}
