package org.elstest.service;

import org.elstest.data.User;
import org.elstest.data.UserReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    private UserReader userReader;

    @Autowired
    public UserService(UserReader userReader){

        this.userReader = userReader;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userReader.findUserByName(userName);
        if (user != null)
            return user;
        throw new UsernameNotFoundException("User '" + userName + "' not found");
    }
}
