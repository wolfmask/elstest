function search() {

    // hide results area
    $('.productDetailBlockLabels').css('display', 'none');
    $('.productDetailBlockData').css('display', 'none');

    var productName = $("input[name='productName']").val().trim();
  //  var validChars = /^[\p{L}-.: ]+$/;
 //pattern="^[\w.-:; ]+"
    if (productName=="") {
        alert("Title text must be entered");
        return;
    }

   // if (!validChars.test(productName)) {
  //      alert("Invalid characters in product name");
  //      return;
  //  }

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", '/elstest/search?productname=' + productName, false ); // false for synchronous request
    xmlHttp.send( null );
    var result = JSON.parse(xmlHttp.responseText);

    if (result != null) {

        // display results area
        $('.productDetailBlockLabels').css('display', 'block');
        $('.productDetailBlockData').css('display', 'block');

        $('#notificationType').text(result.notificationType);
        $('#productIdentifier').text(result.productIdentifier);
        $('#productForm').text(result.productForm);
        $('#title').text(result.title);
        $('#contributor').text(result.contributor);
        $('#languageCode').text(result.languageCode);
        $('#imprintName').text(result.imprintName);
        $('#publicationDate').text(result.publicationDate);

    } else {
        alert("Product title not found");
    }
    return;

}

function listProducts() {

    // select correct tab
    openMenu(event, 'List all Products');

    // get data
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", '/elstest/listproducts', false ); // false for synchronous request
    xmlHttp.send( null );
    var result = JSON.parse(xmlHttp.responseText);

    // clear down any title results from previous call
    $('.tempTitle').remove()

    // extract product titles
    for (i = 0; i < result.length; i++){
        $('.productTitleDisplay').append("<p class=\"tempTitle\">" + result[i] + "</p>")

    }

    return;
}

function openMenu(evt, menuItemName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    //tabcontent = $('tabcontent');
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
        //tabcontent[i].css('display', 'none');

    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(menuItemName).style.display = "block";
    evt.currentTarget.className += " active";
}

