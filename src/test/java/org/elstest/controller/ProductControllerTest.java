package org.elstest.controller;

import org.elstest.data.Product;
import org.elstest.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    ProductService productService;

    @Test
    public void testSearchProductFound() throws Exception {

        String title = "A test title";
        Product testProduct = new Product.ProductBuilder(title,"AB")
                .addNotificationType("03").addProductIdentifier("9780000001979").build();

        given(productService.findProductByTitle(title)).willReturn(testProduct);

        mvc.perform(MockMvcRequestBuilders.get("/elstest/search?productname="+title)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.notificationType").value("03"))
                .andExpect(jsonPath("$.productIdentifier").value("9780000001979"));


    }

    @Test
    public void testGetAllProducts() throws Exception {

        List<String> testTitles = Arrays.asList("A new title", "Another one");

        given(productService.getProductTitles()).willReturn(testTitles);

        mvc.perform(MockMvcRequestBuilders.get("/elstest/listproducts")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("[\"A new title\",\"Another one\"]"));

    }



}
