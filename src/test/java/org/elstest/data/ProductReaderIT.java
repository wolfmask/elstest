package org.elstest.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductReaderIT {

    @Autowired
    ProductReader productReader;

    @Test
    public void testPrintAllProductTitles() {

        List<String> listProducts = productReader.getProductTitles();

        assertEquals(25, listProducts.size());
    }

    @Test
    public void testGetProductExactMatch(){
        Product product = productReader.getProductByTitle("CAM Testing: BJ Migration patch 1 2011...");
        assertEquals("9780000001306", product.getProductIdentifier());
    }

    @Test
    public void testGetProductExactMatchDifferentCase(){
        Product product = productReader.getProductByTitle("Cam testing: BJ Migration patch 1 2011...");
        assertEquals("9780000001306", product.getProductIdentifier());
    }

    @Test
    public void testGetProductPartialMatch(){
        Product product = productReader.getProductByTitle("CAM Testing: BJ Migr");
        assertEquals("9780000001306", product.getProductIdentifier());
    }

    @Test
    public void testGetProductPartialMatchDifferentCase(){
        Product product = productReader.getProductByTitle("cam testing: BJ Migr");
        assertEquals("9780000001306", product.getProductIdentifier());
    }

    @Test
    public void testGetProductNoMatch(){
        Product product = productReader.getProductByTitle("no entry here");
        assertNull(product);
    }

    @Test
    public void testGetProductSingleContrib(){
        Product product = productReader.getProductByTitle("CAM Testing: BJ Migr");
        assertEquals("Carne", product.getContributor());
    }

    @Test
    public void testGetProductMultipleContrib(){
        Product product = productReader.getProductByTitle("Fundamentals of Chemistry");
        assertEquals("Brescia/Arents/Meislich", product.getContributor());
    }

}
