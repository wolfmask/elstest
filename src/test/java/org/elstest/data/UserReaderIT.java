package org.elstest.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserReaderIT {

    @Autowired
    private UserReader userReader;

    @Test
    public void testGetAllUsers() {
        List<User> users = userReader.getAllUsers();
        assertEquals(1, users.size());
    }

    @Test
    public void testGetUserMatch(){
        User user = userReader.findUserByName("user");
        assertEquals("user", user.getUsername());
    }

    @Test
    public void testGetUserNoMatch(){
        User user = userReader.findUserByName("notpresent");
        assertNull(user);
    }

}
