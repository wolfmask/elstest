package org.elstest.service;

import org.elstest.data.Product;
import org.elstest.data.ProductReader;

import java.util.List;
import java.util.Arrays;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private ProductReader mockProductReader = mock(ProductReader.class);

    private ProductService productService = new ProductService(mockProductReader);

    @Test
    public void testProductByTitleFound(){

        Product testProduct = new Product.ProductBuilder("A test title","AB")
                .addNotificationType("03").addProductIdentifier("9780000001979").build();

        when(mockProductReader.getProductByTitle(any(String.class))).thenReturn(testProduct);

        assertEquals(testProduct, productService.findProductByTitle("A test title"));

    }

    @Test
    public void testProductByTitleNotFound(){

        Product testProduct = null;

        when(mockProductReader.getProductByTitle(any(String.class))).thenReturn(testProduct);

        assertNull(productService.findProductByTitle("A test title"));

    }

    @Test
    public void testGetAllProductTitles(){

        List<String> testTitles = Arrays.asList("A new title", "Another one");

        when(mockProductReader.getProductTitles()).thenReturn(testTitles);

        assertEquals(testTitles, productService.getProductTitles());
    }
}
