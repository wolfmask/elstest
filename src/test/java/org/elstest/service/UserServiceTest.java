package org.elstest.service;

import org.elstest.data.User;
import org.elstest.data.UserReader;
import org.junit.Test;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class UserServiceTest {

    private UserReader mockUserReader = mock(UserReader .class);

    private UserService userService = new UserService(mockUserReader);

    @Test
    public void testLoadUserByUserNameFound(){

        User testUser = new User("testuser", "testpassword");

        when(mockUserReader.findUserByName(any(String.class))).thenReturn(testUser);

        User result = (User) userService.loadUserByUsername("testuser");
        assertEquals(testUser, result);

    }

    @Test(expected= UsernameNotFoundException.class)
    public void testLoadUserByUserNameNotFound(){

        User testUser = null;

        when(mockUserReader.findUserByName(any(String.class))).thenReturn(testUser);

        User result = (User) userService.loadUserByUsername("testuser");

    }
}
